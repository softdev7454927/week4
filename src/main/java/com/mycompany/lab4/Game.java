/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayers();
                isFinish = true;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                printPlayers();
                isFinish = true;
            }
            table.switchPlayer();
        }
        inputContinue();

    }

    private void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private void printTable() {
        char[][] t = table.getTable();
        System.out.println("*******");
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + "|");
            }
            System.out.println("");
        }
        System.out.println("*******");
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        char[][] t = table.getTable();
        while (true) {
            System.out.print("Please input row and column :");
            int row = kb.nextInt();
            int col = kb.nextInt();
            if (t[row - 1][col - 1] == '-') {
                table.setRowCol(row, col);
                return;
            } else {
                System.out.println("Try again");
            }

        }

    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println("Player " + table.getCurrentPlayer().getSymbol() + " Win!!!");
    }

    private void printDraw() {
        System.out.println("Draw!!!");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void inputContinue() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n): ");
        String newGame = kb.next();
        if (newGame.equalsIgnoreCase("y")) {
            play();
        } else {
            System.out.println("Thank you for playing! Goodbye!");
        }
    }
}
